﻿using System;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests
{
    public class MongoDatabaseFixture: IDisposable
    {
        public readonly IMongoClient _mongoClient;

        public MongoDatabaseFixture()
        {
            _mongoClient = new MongoClient("mongodb://MongoPCF:MongoPCF@localhost"); 
        }

        public void Dispose()
        {
            new MongoDbInitializer(_mongoClient).InitializeDb();
        }
    }
}