﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoClient _mongoClient;

        public MongoDbInitializer(IMongoClient mongoClient)
        {
            _mongoClient = mongoClient;
        }
        
        public void InitializeDb()
        {
            _mongoClient.DropDatabase("PCF");
            var db = _mongoClient.GetDatabase("PCF");
            Fill(db, FakeDataFactory.Employees
                , d => d.Ascending(i => i.Id)
                );
            Fill(db, FakeDataFactory.Roles
                , d => d.Ascending(i => i.Id)
                );

        }

        private void Fill<T>(IMongoDatabase db, List<T> items, params Func<IndexKeysDefinitionBuilder<T>, IndexKeysDefinition<T>>[] indexes)
        {
            var collection = db.GetCollection<T>(typeof(T).Name);

            foreach (var index in indexes)
                collection.Indexes.CreateOne(new CreateIndexModel<T>(index(Builders<T>.IndexKeys)));
            collection.InsertMany(items);
        }
    }
}