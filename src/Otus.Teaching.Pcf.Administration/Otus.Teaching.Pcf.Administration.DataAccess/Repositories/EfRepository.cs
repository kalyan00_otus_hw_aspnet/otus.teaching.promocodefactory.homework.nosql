﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly IMongoClient _mongoClient;
        private IMongoCollection<T> collection =>
             _mongoClient.GetDatabase("PCF").GetCollection<T>(typeof(T).Name);

        public MongoRepository(IMongoClient mongoClient)
        {
            _mongoClient = mongoClient;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await collection.Find(x => true).ToListAsync();

            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await GetFirstWhere(x => x.Id == id);
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await (await collection.FindAsync(x => ids.Contains(x.Id))).ToListAsync();
            return entities;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await(await collection.FindAsync(predicate)).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await (await collection.FindAsync(predicate)).ToListAsync();
        }

        public async Task AddAsync(T entity)
        {
            await collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await collection.ReplaceOneAsync(i => i.Id == entity.Id, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await collection.DeleteOneAsync(i => i.Id == entity.Id);
        }
    }
}